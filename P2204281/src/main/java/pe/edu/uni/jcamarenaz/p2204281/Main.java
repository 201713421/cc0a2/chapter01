/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.jcamarenaz.p2204281;

/**
 *
 * @author Juan Camarena <jcamarenaz@uni.pe>
 */
public class Main {
    
    public static void main(String[] args){
        System.out.println("First project");
        System.out.println("Length: " + args.length);
        System.out.println("args 1: " + args[0]);
        System.out.println("args 2: " + args[1]);
        System.out.println("args 3: " + args[2]);
        int a=Integer.valueOf(args[0]);
        int b=Integer.valueOf(args[1]);
        int c=Integer.valueOf(args[2]);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);  
        System.out.println(args[0]+args[1]);
        System.out.println(a+b);
        System.out.println("Suma: "+(a+b));
        System.out.println(a+b+" unidades");
        
        System.out.println("Arithmetic operators:");
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);
        
        System.out.println("Unary operators:");
        System.out.println(a);
        System.out.println(a++);
        System.out.println(a);
        System.out.println(a--);
        System.out.println(a);
        
        System.out.println("Relational operators:");
        System.out.println(a==b);
        System.out.println(a!=b);
        System.out.println(a>b);
        System.out.println(a>=b);
        System.out.println(a<b);
        System.out.println(a<=b);
        
        System.out.println("Bitwise operators:");
        System.out.println(4&7);
        
        System.out.println("Logical operators:");
        boolean b1=true;
        boolean b2=false;
        System.out.println(b1&&b2);
        System.out.println(b1||b2);
        System.out.println(b1&&!b2);
        System.out.println(!b1||b2);
        
        System.out.println("Assignment operators:");
        int d=4;
        System.out.println(d);
        d+=8;
        System.out.println(d);
        d-=2;
        System.out.println(d);
        
        System.out.println("Conditional operators:");
        int result=(a==b) ? 1 : 2;
        System.out.println("result: "+result);
        a=5;
        b=4;
        result=(--a==b) ? 1 : 2;
        System.out.println(result);
    }
}
