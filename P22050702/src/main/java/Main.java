
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Main {
    public static void main (String[] args) throws FileNotFoundException, IOException{
        System.out.println("Files");
        
        String name="ByteStream.txt";
        File file=new File(name);
        System.out.println("Location of file: "+file.getAbsolutePath());
        System.out.println("Writting a file");
        FileOutputStream fileOutputStream=new FileOutputStream(file);
        fileOutputStream.write(65);
        fileOutputStream.write(77);
        fileOutputStream.write(79);
        fileOutputStream.write(71);
        fileOutputStream.write(85);
        fileOutputStream.write(83);
        fileOutputStream.close();
        
        System.out.println("Read the file");
        FileInputStream fileInputStream=new FileInputStream(file);
        int decimal;
        while((decimal=fileInputStream.read())!=-1){
            System.out.print((char)decimal);
        }
        System.out.println("");
        
        name="CharacterStream.txt";
        file=new File(name);
        System.out.println("Location of file: "+file.getAbsolutePath());
        FileWriter fileWriter=new FileWriter(file);
        fileWriter.write(65);
        fileWriter.write(77);
        fileWriter.write(79);
        fileWriter.write(71);
        fileWriter.write(85);
        fileWriter.write(83);
        fileWriter.close();
        
        System.out.println("Read the file");
        FileReader fileReader=new FileReader(file);
        while((decimal=fileReader.read())!=-1){
            System.out.print((char)decimal);
        }
        System.out.println("");
        
        System.out.println("List of files");
        String paths[];
        file=new File("./");
        paths=file.list();
        for (String path:paths) {
            System.out.println(path);
        }
        
        System.out.println("Creating directory");
        String directory="/Files/Binaries/Selected";
        String fullPath=file.getAbsolutePath()+directory;
        file=new File(fullPath);
        if (file.mkdirs()) {
            System.out.println("Directories has been created");
        } else {
            System.out.println("Directories has already been created");
        }
    }
}
