/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.jcamarenaz.p22051401;

/**
 *
 * @author Juan Camarena <jcamarenaz@uni.pe>
 */
public class Main {
    public static void main (String[] args) {
        System.out.println("Exceptions ...");
        int[] array = {10, 20, 30, 40, 50, 60};
        try {
            System.out.println(array[8]);
            System.out.println("End of try ...");
            
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index error");
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            int i = 5;
            System.out.println(array[i]);
            int j = i/0;
            Function();
            
            System.out.println("End of try ...");
        } catch (ArithmeticException e) {
            System.out.println("/ by zero");
            System.out.println(e);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index error");
            System.out.println(e);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            System.out.println("Close DB connection, even if the query is not completed");
        }
        System.out.println("End of application ...");
    }
    
    public static void Function() throws ArithmeticException {
        int j = 1/0;
        throw new ArithmeticException();
        
    }
}
