/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.jcamarenaz.p22043001;

/**
 *
 * @author Juan Camarena <jcamarenaz@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Loop statements");
        System.out.println("For loop");
        for(int i=0;i<10;i++) {
            System.out.println(i);
        }
        for(int i=9;i>=0;i--){
            System.out.println(i);
        }
        int f=5;
        int c=3;
        for(int i=1;i<=f;i++){
            for(int j=1;j<=c;j++){
                System.out.print("("+i+","+j+")"+'\t');
            }
            System.out.println("");
        }
        
        System.out.println("While loop");
        int i=0;
        while(i<10) {
            i++;
            System.out.println(i);
        }
        
        i=10;
        while(i>0){
            System.out.println(i);
            i--;
        }
        
        i=1;
        int j;
        while(i<=f){
            j=1;
            while(j<=c){
                System.out.print("("+i+","+j+")"+'\t');
                j++;
            }
            System.out.println("");
            i++;
        }
        
        System.out.println("Interation");
        i=0;
        while(i<0){
            System.out.println("This message is not printed");
        }
        
        do{
            System.out.println("This do while message is printed");
        } while(i<0);
        
        i=1;
        do{
            j=1;
            do{
                System.out.print("("+i+","+j+")"+'\t');
                j++;
            } while(j<=c);
            System.out.println("");
            i++;
        } while(i<=f);
        
        for(int p=10;p<=90;p++){
            boolean b=false;
            for(int k=2;k<p;k++){
                if(p%k==0){
                    b=true;
                    break;
                }
            }
            if(b==false) {
                System.out.println(p+" is prime");
            }
        }
    }
}
