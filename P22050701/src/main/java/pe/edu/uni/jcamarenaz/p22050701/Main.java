/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.jcamarenaz.p22050701;

/**
 *
 * @author User
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Functions");
        Function();
        Function("Juan");
        Function("Juan",22);
        String[] result={"tu","no","mete","cabra","saramambiche"};
        Function(result);
        int sum=Function(1,2,3,4,5,6,7,8,9,10);
        System.out.println("sum: "+sum);
        int a=2;
        int b=3;
        sum=Function(a,b);
        System.out.println("a: "+String.valueOf(a)+", b: "+String.valueOf(b)+", sum: "+String.valueOf(sum));
        StringBuilder stringNumber=new StringBuilder();
        stringNumber.append("0");
        System.out.println("StringNumber: "+stringNumber);
        Function(stringNumber);
        System.out.println("StringNumber: "+stringNumber);
        stringNumber.setLength(0);
        if (authentication("8",stringNumber)) {
            System.out.println("Authentication is ok !!!");
        } else {
            System.out.println("Authentication failed, error: "+stringNumber);
        }
    }
    
    public static void Function() {
        System.out.println("Hello world !!!");
    }
    
    public static void Function(String name) {
        System.out.println("Hello "+name+" !!!");
    }
    
    public static void Function(String name, int age) {
        System.out.println("Hello "+name+" you are "+String.valueOf(age)+" years old ");
    }
    
    public static void Function(String[] args){
        for(String arg:args){
            System.out.print(arg+" ");
        }
        System.out.println("");
    }
    
    public static int Function(int... numbers) {
        int sum=0;
        for(int number:numbers) {
            sum+=number;
        }
        return sum;
    }
    
    public static void Function(StringBuilder reference) {
        reference.setLength(0);
        reference.append("1");
    }
    
    public static boolean authentication (String credential,StringBuilder error) {
        boolean out=false;
        error.setLength(0);
        if (credential.equals("1")){
            out=true;
            error.append("0");
        } else {
            error.append("-1");
        }
        return out;
    }
}
